(function() {
  var _browser = null;
  if (typeof browser !== 'undefined') {
    _browser = browser;
  }
  if (typeof chrome !== 'undefined') {
    _browser = chrome;
  }
  if (_browser === null) {
    throw new Error("Can't initialize");
  }
  let payload = {
    ts: (new Date()).toISOString()
    page: {
      location: location.toString(),
      title: document.title
    },
  }
  console.log(">> HistorySink", payload)
  _browser.storage.local.get(
    'apiBase',
    (result) => {
      let apiBase = result.apiBase
      if (apiBase) {
        fetch({
          method: 'POST',
          body: payload
        })
      }
    }
  )
})()
